const dnssd = require("dnssd");

const config = require("../config.json");

const SERVICE_NAME = config.dnssd.service;
const PORT = config.websocket.port;

let DISPLAY_NAME;

if (config.dnssd["display_name"]) {
  DISPLAY_NAME = config.dnssd["display_name"];
} else {
  DISPLAY_NAME = `${SERVICE_NAME}_0xTODO:SerialNumber`;
}

const InitService = _ => {
// advertise a http server on port 4321
  const ad = new dnssd.Advertisement(dnssd.tcp(SERVICE_NAME), PORT, {
    name: DISPLAY_NAME,
  });
  ad.start();

  process.on("exit", code => {
    ad.stop();
    setTimeout(process.exit, 200);
  });
  process.on("SIGINT", code => {
    ad.stop();
    setTimeout(process.exit, 200);
  });

  return ad;
};


module.exports = InitService;

