const EE = require("events");
const WebSocket = require("ws");
const AccessorySdk = require("./accessorysdk");

const config = require("../config.json");

const PORT = config.websocket.port;
const CHECK_ALIVE_INTERVAL = config.websocket.check_alive_interval;

// params for accessory sdk
const REDIS_PARAMS = config.ipc.redis;
const JOB_CHANNEL = config.ipc.job_channel;
const BCAST_CHANNEL = config.ipc.broadcast_channel;

const BobaosKitWs = _ => {
  const self = new EE();
  const wss = new WebSocket.Server({port: PORT});
  wss.on("listening", _ => {
    console.log(`Websocket is listening on port ${PORT}`);
    self.emit("listening");
  });
  wss.on("connection", ws => {
    ws.isAlive = true;
    ws.on("pong", _ => {
      ws.isAlive = true;
    });

    // init responseProxyHandler and sendResponse function outside message event
    const sendResponse = response => {
      return _ => {
        let dataToSend = {};
        dataToSend["response_id"] = response["response_id"];
        dataToSend["method"] = response["method"];
        dataToSend["payload"] = response["payload"];

        return ws.send(JSON.stringify(dataToSend));
      };
    };
    const responseProxyHandler = {
      ownKeys: target => {
        return ["method", "payload"];
      },
      get: (obj, prop, receiver) => {
        // res.send(data);
        if (prop === "send") {
          return sendResponse(obj);
        }
        if (prop === "method" || prop === "payload") {
          return obj[prop];
        }

        // we don't want to return other props and methods yet
        return null;
      },
      set: (obj, prop, value, receiver) => {
        if (prop === "method" || prop === "payload") {
          obj[prop] = value;
        }
      }
    };
    ws.on("message", message => {
      try {
        let parsed = JSON.parse(message);
        let hasRequestId = Object.prototype.hasOwnProperty.call(parsed, "request_id");
        let hasMethodField = Object.prototype.hasOwnProperty.call(parsed, "method");
        let hasPayloadField = Object.prototype.hasOwnProperty.call(parsed, "payload");
        if (!hasMethodField) {
          let dataToSend = {};
          dataToSend["response_id"] = parsed["request_id"];
          dataToSend["method"] = "error";
          dataToSend["payload"] = "Request should have method field";

          return ws.send(JSON.stringify(dataToSend));
        }
        // if (!hasPayloadField) {
        //   let dataToSend = {};
        //   dataToSend["response_id"] = parsed["request_id"];
        //   dataToSend["method"] = "error";
        //   dataToSend["payload"] = "Request should have payload field";
        //
        //   return ws.send(JSON.stringify(dataToSend));
        // }
        // request should have request_id, method and payload fields
        // otherwise there will be no response
        if (hasRequestId && hasMethodField) {
          // request id, request obj, request proxy to expose
          let request_id = parsed["request_id"];
          let request = {
            method: parsed.method,
            payload: null
          };
          if (hasPayloadField) {
            request.payload = parsed.payload;
          }
          let requestProxy = new Proxy(request, {
            ownKeys: target => {
              return ["method", "payload",];
            },
            get: (obj, prop, receiver) => {
              if (prop === "method" || prop === "payload" || prop === "response_channel") {
                return obj[prop];
              }

              return null;
            }
          });

          let response = {};
          response["response_id"] = request_id;

          const responseProxy = new Proxy(response, responseProxyHandler);
          self.emit("request", requestProxy, responseProxy);
        }
      } catch (e) {
        console.log(`Request error: ${e.message}`);
        let dataToSend = {};
        dataToSend.method = "error";
        dataToSend.payload = e.message;
        ws.send(JSON.stringify(dataToSend));
      }
    });
  });

  const checkAliveInterval = setInterval(_ => {
    wss.clients.forEach(ws => {
      if (!ws.isAlive) {
        return ws.terminate();
      }

      ws.isAlive = false;
      return ws.ping();
    })
  }, CHECK_ALIVE_INTERVAL);

  // broadcasting
  self.broadcast = data => {
    return wss.clients.forEach(ws => {
      return ws.send(data);
    })
  };

  return self;
};

const InitLib = _ => {
  const self = new EE();
  const wsEmitter = BobaosKitWs();
  wsEmitter.on("listening", _ => {
    self.emit("listening");
  });
  const accessorySdk = AccessorySdk({
    redis: REDIS_PARAMS,
    job_channel: JOB_CHANNEL,
    broadcast_channel: BCAST_CHANNEL
  });
  accessorySdk.on("broadcasted event", (method, payload) => {
    const data2send = JSON.stringify({method: method, payload: payload});
    wsEmitter.broadcast(data2send);
  });
  wsEmitter.on("request", (req, res) => {
    let {method, payload} = req;
    if (method === "ping") {
      return accessorySdk
        .ping()
        .then(result => {
          res.method = "success";
          res.payload = result;
          res.send();
        })
        .catch(e => {
          res.method = "error";
          res.payload = e.message;
          res.send();
        });
    }
    if (method === "get general info") {
      return accessorySdk
        .getGeneralInfo()
        .then(result => {
          res.method = "success";
          res.payload = result;
          res.send();
        })
        .catch(e => {
          res.method = "error";
          res.payload = e.message;
          res.send();
        });
    }
    if (method === "get accessory info") {
      return accessorySdk
        .getAccessoryInfo(payload)
        .then(result => {
          res.method = "success";
          res.payload = result;
          res.send();
        })
        .catch(e => {
          res.method = "error";
          res.payload = e.message;
          res.send();
        });
    }
    if (method === "get status value") {
      return accessorySdk
        .getStatusValue(payload)
        .then(result => {
          res.method = "success";
          res.payload = result;
          res.send();
        })
        .catch(e => {
          res.method = "error";
          res.payload = e.message;
          res.send();
        });
    }
    if (method === "control accessory value") {
      return accessorySdk
        .controlAccessoryValue(payload)
        .then(result => {
          res.method = "success";
          res.payload = result;
          res.send();
        })
        .catch(e => {
          res.method = "error";
          res.payload = e.message;
          res.send();
        });
    }

    res.method = "error";
    res.payload = "unknown method";
    res.send();
  });

  return self;
};

module.exports = InitLib;