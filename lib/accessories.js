const EE = require("events");
const Redis = require("redis");
const Queue = require("bee-queue");

const config = require("../config.json");

const BOBAOSKIT_ID = config.id;
const REDIS_PARAMS = config.ipc.redis;
const JOB_CHANNEL = config.ipc.job_channel;
const BCAST_CHANNEL = config.ipc.broadcast_channel;

const KEY_PREFIX = `${config.ipc.key_prefix}:accessories`;
const KEY_COUNT = `${KEY_PREFIX}_count`;

let globalQueueArray = [];

const Init = _ => {
  let self = new EE();

  const rc = Redis.createClient(REDIS_PARAMS);

  self.getGeneralInfo = cb => {
    let info = {
      id: BOBAOSKIT_ID,
      job_channel: JOB_CHANNEL,
      bcast_channel: BCAST_CHANNEL,
      key_prefix: KEY_PREFIX
    };
    rc.get(KEY_COUNT, (err, res) => {
      if (err) {
        return cb(err);
      }

      info.acc_count = JSON.parse(res);
      cb(null, info);
    });
  };

  self.clearAccessories = cb => {
    let keyPattern = `${KEY_PREFIX}:*`;
    rc.keys(keyPattern, (err, res) => {
      if (err) {
        return cb(err);
      }

      const m = rc.multi();
      // delete all accessories keys
      res.forEach(k => {
        m.del(k);
      });
      // set count to 0
      m.set(KEY_COUNT, 0);
      m.exec((err, res) => {
        if (err) {
          return cb(err);
        }

        return cb(null, res);
      });
    });
  };

  self.addAccessory = (params, cb) => {
    const m = rc.multi();
    try {
      // name: Name
      // type: Accessory type, eg switch
      // fields: Available fields for selected accessory type
      // service: mapping for field-datapoint
      let { name, id, type, control, status } = params;
      if (typeof name === "undefined") {
        throw new Error("Accessory name is undefined");
      }
      if (typeof id === "undefined") {
        throw new Error("Accessory id is undefined");
      }
      if (typeof type === "undefined") {
        throw new Error("Accessory type is undefined");
      }
      if (typeof control === "undefined") {
        throw new Error("Accessory control fields is undefined");
      }
      if (typeof status === "undefined") {
        throw new Error("Accessory status fields is undefined");
      }
      if (!(Array.isArray(control) || Buffer.isBuffer(control))) {
        throw new TypeError("Accessory control fields must be array");
      }
      if (!(Array.isArray(status) || Buffer.isBuffer(status))) {
        throw new TypeError("Accessory status fields must be array");
      }
      if (control.length === 0) {
        throw new Error("Accessory control fields array is empty");
      }
      if (status.length === 0) {
        throw new Error("Accessory status fields array is empty");
      }
      const ACC_PREFIX = `${KEY_PREFIX}:${id}`;
      const ACC_INFO_KEY = `${ACC_PREFIX}:info`;
      const ACC_CTRL_KEY = `${ACC_PREFIX}:control`;
      const ACC_STAT_KEY = `${ACC_PREFIX}:status`;
      // check is accessory with given id exists
      rc.exists(ACC_INFO_KEY, (err, res) => {
        if (err) {
          return cb(err);
        }

        if (res) {
          return cb(new Error("Accessory with given id already exists"));
        }

        // accessory receive info in itself, then creates queue listening job_channel
        // on incoming control requests for this accessory bobaos.kit sends request to job_channel
        // accessory receives request then sends request to bobaos.
        // therefore, create new Queue object for accessory and send requests using it.
        const JOB_CHANNEL = `${ACC_PREFIX}:jobs`;

        // DONE: create queue object and push in global array
        const accessoryJobQueue = new Queue(JOB_CHANNEL, {
          redis: config.ipc.redis,
          isWorker: false
        });
        globalQueueArray.push({ channel: JOB_CHANNEL, object: accessoryJobQueue });

        const controlArr = Array.prototype.slice.call(control);
        const statusArr = Array.prototype.slice.call(status);
        let accessoryInfoObject = {
          id: id,
          job_channel: JOB_CHANNEL,
          name: name,
          type: type,
          control: controlArr,
          status: statusArr
        };
        let accessoryControlFieldsHashArr = [];
        controlArr.forEach(f => {
          accessoryControlFieldsHashArr.push(f);
          accessoryControlFieldsHashArr.push(0);
        });
        let accessoryStatusFieldsHashArr = [];
        status.forEach(f => {
          accessoryStatusFieldsHashArr.push(f);
          accessoryStatusFieldsHashArr.push(0);
        });

        const accessoryInfoStr = JSON.stringify(accessoryInfoObject);

        // const fieldsBuff = Buffer.from(fields, "base64");
        m.set(ACC_INFO_KEY, accessoryInfoStr);
        m.hmset(ACC_CTRL_KEY, accessoryControlFieldsHashArr);
        m.hmset(ACC_STAT_KEY, accessoryStatusFieldsHashArr);
        m.incr(KEY_COUNT);

        m.exec((err, res) => {
          if (err) {
            return cb(err);
          }

          rc.get(ACC_INFO_KEY, (err, res) => {
            if (err) {
              return cb(err);
            }

            let result = JSON.parse(res);
            return cb(null, result);
          });
        });
      });
    } catch (err) {
      return cb(err);
    }
  };

  self.removeAccessory = (id, cb) => {
    const removeOneAccessory = (id, cb) => {
      if (typeof id !== "number" && typeof id !== "string") {
        return cb(new TypeError("Accessory id should be number or string"));
      }
      const ACC_PREFIX = `${KEY_PREFIX}:${id}`;
      const ACC_INFO_KEY = `${ACC_PREFIX}:info`;
      const ACC_CTRL_KEY = `${ACC_PREFIX}:control`;
      const ACC_STAT_KEY = `${ACC_PREFIX}:status`;
      rc.exists(ACC_INFO_KEY, (err, res) => {
        if (err) {
          return cb(err);
        }

        if (res) {
          const m = rc.multi();
          m.del(ACC_INFO_KEY);
          m.del(ACC_CTRL_KEY);
          m.del(ACC_STAT_KEY);
          m.decr(KEY_COUNT);
          m.exec((err, res) => {
            if (err) {
              return cb(err);
            }

            cb(null, true);
          });
        } else {
          cb(new Error(`There is no registered accessory with id '${id}'.`));
        }
      });
    };

    if (Array.isArray(id)) {
      let eachAsync = (arr, func, cb) => {
        let doneCounter = 0;
        let results = [];
        let errors = [];
        arr.forEach(p => {
          func(p, (err, res) => {
            doneCounter += 1;
            if (err) {
              errors.push(err.message);
            } else {
              results.push(res);
            }
            if (doneCounter === arr.length) {
              if (errors.length > 0) {
                console.log(errors);
                return cb(new Error(JSON.stringify(errors)));
              }

              cb(null, results);
            }
          });
        });
      };
      return eachAsync(id, removeOneAccessory, cb);
    }

    removeOneAccessory(id, cb);
  };

  self.getAccessoryInfo = (id, cb) => {
    const _getOneAccessoryInfo = (id, cb) => {
      const ACC_PREFIX = `${KEY_PREFIX}:${id}`;
      const ACC_INFO_KEY = `${ACC_PREFIX}:info`;
      const ACC_CTRL_KEY = `${ACC_PREFIX}:control`;
      const ACC_STAT_KEY = `${ACC_PREFIX}:status`;
      rc.get(ACC_INFO_KEY, (err, res) => {
        if (err) {
          return cb(err);
        }

        let result = JSON.parse(res);
        return cb(null, result);
      });
    };

    if (Array.isArray(id)) {
      const multi = rc.multi();
      id.forEach(i => {
        const ACC_PREFIX = `${KEY_PREFIX}:${i}`;
        const ACC_INFO_KEY = `${ACC_PREFIX}:info`;
        const ACC_CTRL_KEY = `${ACC_PREFIX}:control`;
        const ACC_STAT_KEY = `${ACC_PREFIX}:status`;
        multi.get(ACC_INFO_KEY);
      });
      return multi.exec((err, res) => {
        if (err) {
          return cb(err);
        }

        let result = res.map(JSON.parse);
        cb(null, result);
      });
    }
    if (typeof id === "string" || typeof id === "number") {
      return _getOneAccessoryInfo(id, cb);
    }
    if (id === null) {
      let keyPattern = `${KEY_PREFIX}:*:info`;
      return rc.keys(keyPattern, (err, res) => {
        if (err) {
          return cb(err);
        }
        // get all accs
        const multi = rc.multi();
        res.forEach(key => {
          multi.get(key);
        });
        multi.exec((err, res) => {
          if (err) {
            return cb(err);
          }

          let result = res.map(JSON.parse);
          return cb(null, result);
        });
      });
    }
    cb(new TypeError("Unknown accessory id. Should be 'null', id, or array of id."));
  };

  // TODO: pingAccessory
  // TODO: pingAccessory: find queue, create job, get response

  self.getStatusValue = (payload, cb) => {
    // TODO: check
    const getOneAccessoryValues = (payload, cb) => {
      if (!payload) {
        return cb(new Error("Payload is not defined"));
      }
      if (!Object.prototype.hasOwnProperty.call(payload, "id")) {
        return cb(new Error("Payload object should have 'id' field"));
      }

      const id = payload.id;

      // DONE: refactor: if status is null or not defined then get all values
      let status;
      if (Object.prototype.hasOwnProperty.call(payload, "status")) {
        // return cb(new Error("Payload object should have 'status' field"));
        status = payload.status;
      } else {
        status = null;
      }

      self.getAccessoryInfo(id, (err, res) => {
        if (err) {
          return cb(err);
        }

        if (!res) {
          return cb(new Error(`Empty accessory '${id}'`));
        }

        // check is status object in request is null
        // in this case we want to receive all values from accessory
        if (status === null) {
          status = res.status;
        }

        const ACC_PREFIX = `${KEY_PREFIX}:${id}`;
        const ACC_STAT_KEY = `${ACC_PREFIX}:status`;
        const m = rc.multi();

        const statusMap = [];
        const getOneField = field => {
          if (!res.status.includes(field)) {
            throw new Error(`Field '${field}' is not defined in accessory info`);
          }
          m.hget(ACC_STAT_KEY, field);
          statusMap.push(field);
        };

        try {
          if (Array.isArray(status)) {
            status.forEach(getOneField);
          } else {
            getOneField(status);
          }
        } catch (e) {
          return cb(e);
        }
        m.exec((err, res) => {
          if (err) {
            return cb(err);
          }

          let map = res.map((v, i) => {
            let value;
            try {
              value = JSON.parse(v);
            } catch (e) {
              value = v;
            }

            return {
              field: statusMap[i],
              value: value
            };
          });
          // if only one field
          if (map.length === 1) {
            cb(null, { id: id, status: map[0] });
          } else {
            cb(null, { id: id, status: map });
          }
        });
      });
    };
    if (Array.isArray(payload)) {
      // from stackoverflow async iterator over array
      let eachAsync = (arr, func, cb) => {
        let doneCounter = 0;
        let results = [];
        let errors = [];
        arr.forEach(p => {
          func(p, (err, res) => {
            doneCounter += 1;
            if (err) {
              errors.push(err.message);
            } else {
              results.push(res);
            }
            if (doneCounter === arr.length) {
              if (errors.length > 0) {
                console.log(errors);
                return cb(new Error(JSON.stringify(errors)));
              }

              cb(null, results);
            }
          });
        });
      };
      eachAsync(payload, getOneAccessoryValues, cb);
    } else {
      getOneAccessoryValues(payload, cb);
    }
  };

  self.updateStatusValue = (payload, cb) => {
    const updateOneValue = (payload, cb) => {
      if (!payload) {
        return cb(new Error("Payload is not defined"));
      }
      if (!Object.prototype.hasOwnProperty.call(payload, "id")) {
        return cb(new Error("Payload object should have accessory 'id' field."));
      }
      if (!Object.prototype.hasOwnProperty.call(payload, "status")) {
        return cb(new Error("Payload object should have 'status' field."));
      }

      let { id, status } = payload;
      const ACC_PREFIX = `${KEY_PREFIX}:${id}`;
      const ACC_STAT_KEY = `${ACC_PREFIX}:status`;
      self.getAccessoryInfo(id, (err, res) => {
        if (err) {
          return cb(err);
        }
        if (!res) {
          return cb(new Error(`Can't find info for accessory ${id}`));
        }

        // wrong type of status
        if (typeof status !== "object") {
          return cb(new TypeError("Payload 'status' object should be Array or object"));
        }
        // TODO: refactor ?
        const m = rc.multi();
        const hsetOneValue = s => {
          if (!Object.prototype.hasOwnProperty.call(s, "field")) {
            throw new Error("Status object should have 'field' and 'value' fields");
          }
          if (!Object.prototype.hasOwnProperty.call(s, "value")) {
            throw new Error("Status object should have 'field' and 'value' fields");
          }
          let { field, value } = s;
          if (!res.status.includes(field)) {
            throw new Error(`Field '${field}' is not defined in accessory info`);
          }
          m.hset(ACC_STAT_KEY, field, JSON.stringify(value));
          return m;
        };

        if (Array.isArray(status)) {
          let results = [];
          let errors = [];
          status.forEach(s => {
            try {
              hsetOneValue(s);
            } catch (e) {
              errors.push(e.message);
            }
          });
          if (errors.length > 0) {
            return cb(new Error(JSON.stringify(errors)));
          } else {
            return m.exec((err, res) => {
              if (err) {
                return cb(err);
              }

              cb(null, res);
            });
          }
        }

        try {
          hsetOneValue(status);
          m.exec((err, res) => {
            if (err) {
              return cb(err);
            }

            cb(null, res);
          });
        } catch (e) {
          cb(e);
        }
      });
    };

    if (Array.isArray(payload)) {
      // from stackoverflow async iterator over array
      let eachAsync = (arr, func, cb) => {
        let doneCounter = 0;
        let results = [];
        let errors = [];
        arr.forEach(p => {
          func(p, (err, res) => {
            doneCounter += 1;
            if (err) {
              errors.push(err.message);
            } else {
              results.push(res);
            }
            if (doneCounter === arr.length) {
              if (errors.length > 0) {
                console.log(errors);
                return cb(new Error(JSON.stringify(errors)));
              }

              cb(null, results);
            }
          });
        });
      };
      eachAsync(payload, updateOneValue, cb);
    } else {
      updateOneValue(payload, cb);
    }
  };

  self.controlAccessoryValue = (payload, cb) => {
    //  [../\..]
    const controlOneValue = (payload, cb) => {
      if (!payload) {
        return cb(new Error("Payload is not defined"));
      }
      if (!Object.prototype.hasOwnProperty.call(payload, "id")) {
        return cb(new Error("Payload value object should have accessory 'id' field."));
      }
      if (!Object.prototype.hasOwnProperty.call(payload, "control")) {
        return cb(new Error("Payload value object should have 'control' field."));
      }
      let { id, control } = payload;
      self.getAccessoryInfo(id, (err, res) => {
        if (err) {
          return cb(err);
        }
        if (!res) {
          return cb(new Error(`Can't find info for accessory ${id}`));
        }
        try {
          const { job_channel } = res;
          const findByJobChannel = t => t.channel === job_channel;
          const accessoryQueue = globalQueueArray.find(findByJobChannel).object;

          // check control for array/object type
          if (typeof control !== "object") {
            throw new Error("Control field should be array or object instance");
          }

          // check control for valid field values.
          const _checkOneItem = t => {
            if (!Object.prototype.hasOwnProperty.call(t, "field")) {
              throw new Error("Should have 'field' field");
            }
            if (!Object.prototype.hasOwnProperty.call(t, "value")) {
              throw new Error("Should have 'value' field");
            }
          };
          if (Array.isArray(control)) {
            control.forEach(_checkOneItem);
          } else {
            _checkOneItem(control);
          }

          const jobData = { method: "control accessory value", payload: control };
          // create job but don't care about result, accessory should send status update
          accessoryQueue
            .createJob(jobData)
            .save()
            .then(_ => {
              cb(null, true);
            })
            .catch(cb);
        } catch (e) {
          console.log(e);
          cb(e);
        }
      });
    };

    // multiple values
    if (Array.isArray(payload)) {
      return payload.forEach(t => controlOneValue(t, cb));
    }

    controlOneValue(payload, cb);
  };

  // find already existed accessories and create queue objects for them
  let scanExisting = cb => {
    if (typeof cb !== "function") {
      cb = _ => undefined;
    }
    self.getAccessoryInfo(null, (err, info) => {
      if (err) {
        return cb(err);
      }

      if (Array.isArray(info)) {
        info.forEach(info => {
          const JOB_CHANNEL = info.job_channel;
          const findByJobChannel = t => {
            return t.channel === JOB_CHANNEL;
          };
          // if can't find in global queue array
          if (globalQueueArray.findIndex(findByJobChannel) === -1) {
            const accessoryJobQueue = new Queue(JOB_CHANNEL, {
              redis: config.ipc.redis,
              isWorker: false
            });
            globalQueueArray.push({ channel: JOB_CHANNEL, object: accessoryJobQueue });
          }
        });
        cb(null, true);
      }
    });
  };

  scanExisting();
  return self;
};

module.exports = Init();
