#!/usr/bin/env node

const Ipc = require("../index");

const InitWsLib = require("../lib/ws");
const InitDnsAd = require("../lib/dnssd");

Ipc.on("ready", _ => {
  const ws = InitWsLib();
  ws.on("listening", _ => {
    const ad = InitDnsAd();
    console.log("All services started");
  })
});