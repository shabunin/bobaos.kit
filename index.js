const IPC = require("./lib/ipc");
const Kit = require("./lib/accessories.js");

const Ipc = IPC();

Ipc.on("ready", _ => {
  console.log("IPC queue ready")
});

Ipc.on("request", (req, res) => {
  let {method, payload} = req;
  if (method === "ping") {
    res.method = "success";
    res.payload = true;
    return res.send();
  }
  if (method === "get general info") {
    return Kit.getGeneralInfo((err, result) => {
      if (err) {
        res.method = "error";
        res.payload = err.message;
        return res.send();
      }

      res.method = "success";
      res.payload = result;
      res.send();
    });
  }
  if (method === "clear accessories") {
    return Kit.clearAccessories((err, result) => {
      if (err) {
        res.method = "error";
        res.payload = err.message;
        return res.send();
      }

      res.method = "success";
      res.payload = true;
      res.send();

      Ipc.broadcast({
        method: method,
        payload: payload
      });
    });
  }
  if (method === "add accessory") {
    return Kit.addAccessory(payload, (err, acc) => {
      if (err) {
        res.method = "error";
        res.payload = err.message;
        return res.send();
      }

      res.method = "success";
      res.payload = acc;
      res.send();

      Ipc.broadcast({
        method: method,
        payload: payload
      });
    });
  }
  if (method === "remove accessory") {
    return Kit.removeAccessory(payload, (err, acc) => {
      if (err) {
        res.method = "error";
        res.payload = err.message;
        return res.send();
      }

      res.method = "success";
      res.payload = acc;
      res.send();

      Ipc.broadcast({
        method: method,
        payload: payload
      });
    })
  }
  if (method === "get accessory info") {
    return Kit.getAccessoryInfo(payload, (err, acc) => {
      if (err) {
        res.method = "error";
        res.payload = err.message;
        return res.send();
      }

      res.method = "success";
      res.payload = acc;
      res.send();
    })
  }
  if (method === "get status value") {
    return Kit.getStatusValue(payload, (err, acc) => {
      if (err) {
        res.method = "error";
        res.payload = err.message;
        return res.send();
      }

      res.method = "success";
      res.payload = acc;
      res.send();
    })
  }
  if (method === "update status value") {
    return Kit.updateStatusValue(payload, (err, acc) => {
      if (err) {
        res.method = "error";
        res.payload = err.message;
        return res.send();
      }

      res.method = "success";
      res.payload = acc;
      res.send();

      Ipc.broadcast({
        method: method,
        payload: payload
      });
    })
  }
  if (method === "control accessory value") {
    return Kit.controlAccessoryValue(payload, (err, acc) => {
      if (err) {
        res.method = "error";
        res.payload = err.message;
        return res.send();
      }

      res.method = "success";
      res.payload = acc;
      res.send();
    })
  }

  res.method = "error";
  res.payload = "Unknown method";
  res.send();
});

module.exports = Ipc;