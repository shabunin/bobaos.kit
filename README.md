# bobaoskit.worker - accessory backend

## About

This module intended to be accessory backend.
Originally, bobaos project has been created to control KNX values.
Then idea, inspired by Apple HomeKit and by absense of it's Android implementation brings me here.

## Intro

In the basic, it uses `redis` in-memory database as a backend to store general/accessory information and for job processing with help of `bee-queue` library.

After accessory backend is started, this worker module creates websocket server to communicate with accessories and advertise it via mdns using `dnssd` lib.

## Installation

TODO: npm package

This module is in deep developement, so there is no npm package yet.

Clone this repo, instead, edit `./config.json` and run `./bin/bobaos-kit.js`.

```text
git clone https://github.com/bobaoskit/bobaoskit.worker.git
cd ./bobaoskit.worker
npm install
./bin/bobaos-kit.js
```

## [Bee-Queue jobs/redis broadcast] API

There is implemented library `bobaoskit.accessory` for this job. But if you want to know how accessories is interacting with worker, then you might study.

All jobs can be sent via `bee-queue` library to job channel defined in `./config.json`. By default it is `bobaoskit_job`.

Job data is an object with defined `method` and `payload` fields.

List of possible operations:

1. method: `ping`, payload: `null`.
2. method: `get general info`, payload: `null`
3. method: `clear accessories`, payload: `null`,
4. method: `add accessory`, 
payload: 

```js
{
  id: "accessoryId",
  type: "switch/sensor/etc",
  name: "Accessory Display Name",
  control: [<array of control fields>],
  status: [<array of status fields>]
}
```
5. method: `remove accessory`, payload: `accessoryId/[acc1id, acc2id, ...]`
6. method: `get accessory info`, payload: `null/accId/[acc1id, acc2id...]`
if payload has `null` value then all accessories will be returned.
7. method: `get status value`, payload: `{id: accessoryId, status: fieldId/[field1, field2...]}`
8. method: `update status value`, payload: `{id: accessoryId, status: {field: fieldId, value: value} /[{valueObj1...}, {}]}`
9. method: `control accessory value`, payload: `{id: accessoryId, control: {field: fieldId, value: value} /[{valueObj1...}, {}]}`

So, accessory register itself by method "add accessory". If accessory was already defined, then error will be returned. So, `bobaoskit.accessory` library first uses `get accessory info` method to check if accessory was defined, then remove it and register again if info is not empty, else it register new accessory. In case of success `bobaos.worker` responds with accessory information, containing `job_channel` field. Bee-queue worker for this job channel is created then on accessory side and on incoming job `bobaoskit.accessory` accessory instance emit `control accessory value` event for `control accessory value` request.

Process incoming request, then send `update status value` request back to `bobaoskit.worker`. So values will be stored in redis database.

On incoming value from control device(e.g. KNX value has updated on bus) use `update status value` again to update status field value.

Broadcasted events, e.g. when accessory updates its value, accessories was cleared or accessory was added/removed is published to redis publish channel defined in `./config.json`.

Published message is json string, containing two fields: `method` and `payload`.

## Creating first accessory

Use library: https://github.com/bobaoskit/bobaoskit.accessory

Free to experiment. There is no any specification for accessories. So, it is possible to register accessories with custom `type`, `control` and `status` fields. You may use number/strings for defining those fields. Also, values is dynamic typed.

## Websocket API

### Discovery

This app uses `dnssd` library to advertise itself on local network.
Edit `./config.json` to setup display name and service name.

After that, you will be able to discover service via `bonjour/avahi/zeroconf`, get Websocket port information and resolve hostname.

### API

This app listens Websocket on port, defined in `./config.json` file for incoming requests.

Request must have `request_id`, `method` and `payload` fields, otherwise you may not receive response.

API is restricted to following methods:

1. method: `ping`, payload: `null`
2. method: `get general info`, payload: `null`,
3. method: `get accessory info`, payload: `null/accId/[acc1Id, ...]`
4. method: `get status value`, payload: `{id: accId, status: field1/[field1, ...]}/[{id: ...}...]`
5. method: `control accessory value`, payload: `{id: accId, control: {field: field1, value: value}/[{field: .. value: ..}]}/[{id: ...}, ...]`

Method `get accessory info` accepts `null` as a payload to get all accessories, single accessory id or array of accessory ids.

Methods `get status value`, `control accessory value` accepts single payload object or array of payload objects to control/get values of multiple accessories. Fields `control`/`status` in each payload object may be single [object/accessory id]  or array too to control/get multiple fields at once.

There is also broadcasted events sent from worker to clients. They are json strings containing `method` and `payload` fields.

Those events may be:

1. method: `clear accessories`, payload: `null`
2. method: `remove accessory`, payload: `accessory id`
3. method: `add accessory`, payload: `{id: ...}`
4. method: `update status value`, payload: `{id: ...}`

Note: please keep in mind that there is no websocket client library for bobaos.pub yet implemented.

## Summary

This app is a simple backend for accessory system. It is really simple. No pairing/authorization is implemented, values can be controlled/get only by `status`/`control` fields.
